import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import './ListItem.css';

function ListItem({ flight }) {
  const getTime = (miliseconds) => {
    if (!miliseconds) return '';
    const x = moment.unix(miliseconds).format('HH:mm');
    return x;
  };

  const getStatusColor = (fl) => {
    let className = '';
    if (fl.details) {
      switch (fl.details.status.generic.status.color) {
        case 'green':
          className = 'bg-success text-white';
          break;
        case 'yellow':
          className = 'bg-warning text-dark';
          break;
        case 'red':
          className = 'bg-danger text-white';
          break;
        default:
          className = 'bg-light text-dark';
      }
    }
    return className;
  }

  const isOnGround = !flight.details.time.real.departure;
  const isLanded = flight.details.time.real.arrival !== null;
  let statusText = 'ON AIR';
  if (isOnGround) {
    statusText = 'NOT DEPARTED';
  }
  else if (isLanded) {
    statusText = 'LANDED';
  }

  let departTime = '';
  let estimatedArrivalTime = '';
  let arrivalTime = '';

  if (flight.details) {
    departTime = getTime(flight.details.time.real.departure);
    estimatedArrivalTime = getTime(flight.details.time.estimated.arrival);
    arrivalTime = getTime(flight.details.time.real.arrival);
  }

  return (
    // eslint-disable-next-line react/jsx-filename-extension
    <tr key={flight.id} className={getStatusColor(flight)}>
      <th scope="row" className="flight-info-container">
        <div>
          <a href={`https://www.flightradar24.com/data/flights/${flight.cs}`} target="_blank" rel="noopener noreferrer">
            {flight.cs}
          </a>
        </div>
        <div className="small">
          <span title="Scheduled">
            {/* <img src="https://img.icons8.com/ios/16/000000/overtime.png" alt="" className="status-icon" /> */}
            { flight.details.time.scheduled.departure > 0 && (
              <span>
                {flight.details ? getTime(flight.details.time.scheduled.departure) : ''}
                -
                {flight.details ? getTime(flight.details.time.scheduled.arrival) : ''}
              </span>
            )}
          </span>
        </div>
      </th>
      <td className="airport-info" title={flight.details ? flight.details.aircraft.model.text : ''}>
        <strong>
          <a href={`https://www.flightradar24.com/data/aircraft/${flight.reg}`} target="_blank" rel="noopener noreferrer">
            {flight.reg}
          </a>
        </strong>
        <br />
        <strong>{flight.plane}</strong>
        <div className="small">{flight.details ? flight.details.aircraft.model.text : ''}</div>
      </td>
      <td className="d-none d-lg-block">
        <a href={(flight.details && flight.details.aircraft.images && flight.details.aircraft.images.thumbnails) ? flight.details.aircraft.images.thumbnails[0].link : ''} target="_blank" rel="noopener noreferrer">
          <img src={(flight.details && flight.details.aircraft.images && flight.details.aircraft.images.thumbnails) ? flight.details.aircraft.images.thumbnails[0].src : ''} alt="..." className="img-thumbnail plane-photo" />
        </a>
      </td>
      <td className="airport-info" title={flight.details ? flight.details.airport.origin.name : ''}>
        <strong>
          <a href={`https://www.flightradar24.com/airport/${flight.from}`} target="_blank" rel="noopener noreferrer">
            {flight.from}
          </a>
        </strong>
        <div className="small">{flight.details ? flight.details.airport.origin.name : ''}</div>
      </td>
      <td className="airport-info" title={(flight.details && flight.details.airport.destination) ? flight.details.airport.destination.name : ''}>
        <strong>
          <a href={`https://www.flightradar24.com/airport/${flight.to}`} target="_blank" rel="noopener noreferrer">
            {flight.to}
          </a>
        </strong>
        <div className="small">{(flight.details && flight.details.airport.destination) ? flight.details.airport.destination.name : ''}</div>
      </td>
      <td>
        <div className="status-container">
          <div>
            <span><strong><a href={`https://www.flightradar24.com/${flight.cs}/${flight.id}`} target="_blank" rel="noopener noreferrer">{statusText}</a></strong></span>
          </div>
          { !isOnGround &&
            // eslint-disable-next-line react/jsx-wrap-multilines
            <div>
              <span title="Departed">
                <img src="https://img.icons8.com/android/16/000000/airplane-take-off.png" alt="" className="status-icon" />
                {departTime}
              </span>
              &nbsp;
              <span title={arrivalTime ? 'Arrived' : 'Estimated arrival'}>
                <img src="https://img.icons8.com/android/16/000000/airplane-landing.png" alt="" className="status-icon" />
                { arrivalTime || estimatedArrivalTime }
              </span>
            </div>
          }
          {/* <span title="Scheduled">
            <img src="https://img.icons8.com/ios/16/000000/overtime.png" alt="" className="status-icon" />
            {flight.details ? getTime(flight.details.time.scheduled.departure) : ''}
            -
            {flight.details ? getTime(flight.details.time.scheduled.arrival) : ''}
          </span> */}
        </div>
      </td>
    </tr>
  )
}

ListItem.propTypes = {
  flight: PropTypes.object,
};

export default ListItem;
