import React, { useState, useEffect } from 'react';
import moment from 'moment';
import Loader from 'react-loader-spinner';
import setQueryString from 'set-query-string';
import './List.css';
import ListItem from './ListItem';
import getQueryStringParams from './lib/queryStringParams';

// TODO zrobic z environment variables
// const FLIGHTS_API = 'http://localhost:3001/flights?callsign=';
const FLIGHTS_API = 'https://flights.msdev.ovh/flights?callsign=';

const AIRLINES = [
  { code: 'lot', label: 'LOT Polish Airlines', logo: 'https://cdn.flightradar24.com/assets/airlines/logotypes/LO_LOT.png' },
  { code: 'iga', label: 'Skytaxi' },
  { code: 'ent', label: 'Enter Air', logo: 'https://www.flightradar24.com/static/images/data/operators/ENT_logo0.png' },
  { code: 'plf', label: 'Polish Air Force' },
  { code: 'bti', label: 'Air Baltic', logo: 'https://images.flightradar24.com/assets/airlines/logotypes/BT_BTI.png' },
  { code: 'bru', label: 'Belavia', logo: 'https://images.flightradar24.com/assets/airlines/logotypes/B2_BRU.png' },
  { code: 'fin', label: 'Finnair', logo: 'https://cdn.flightradar24.com/assets/airlines/logotypes/AY_FIN.png' },
  { code: 'csa', label: 'Czech Airlines', logo: 'https://images.flightradar24.com/assets/airlines/logotypes/OK_CSA.png' },
  { code: 'aui', label: 'Ukraine International Airlines', logo: 'https://www.flightradar24.com/static/images/data/operators/AUI_logo0.png' },
  { code: 'aua', label: 'Austrian Airlines', logo: 'https://images.flightradar24.com/assets/airlines/logotypes/OS_AUA.png' },
  { code: 'aee', label: 'Aegean Airlines', logo: 'https://images.flightradar24.com/assets/airlines/logotypes/A3_AEE.png' },
  { code: 'aza', label: 'Alitalia', logo: 'https://images.flightradar24.com/assets/airlines/logotypes/AZ_AZA.png' },
  { code: 'swr', label: 'Swiss', logo: 'https://images.flightradar24.com/assets/airlines/logotypes/LX_SWR.png' }

];

function List() {
  const queryParams = getQueryStringParams();
  const initialAirline = queryParams.airline;
  const initialAircraft = queryParams.aircraft;
  const initialFilterText = queryParams.filter;

  // console.log(`Initial filters. Airline: ${initialAirline}, Aircraft: ${initialAircraft}`);

  const [initialFlights, setInitialFlights] = useState([]);
  const [flights, setFlights] = useState([]);
  const [greenFlightsCount, setGreenFlightsCount] = useState(0);
  const [yellowFlightsCount, setYellowFlightsCount] = useState(0);
  const [redFlightsCount, setRedFlightsCount] = useState(0);
  const [airlineCode, setAirlineCode] = useState(initialAirline || AIRLINES[0].code);
  const [airlineLogo, setAirlineLogo] = useState('');
  const [filterText, setFilterText] = useState('');
  const [filterAircraft, setFilterAircraft] = useState('');
  const [aircraftTypes, setAircraftTypes] = useState([]);
  const [lastFetch, setLastFetch] = useState(null);
  const [loading, setLoading] = useState(false);

  // const handleRefresh = e => {
  //   console.log("filter handler", e);
  //   e.preventDefault();
  //   //fetchData();
  // };

  const setFilterTextExt = (val) => {
    setFilterText(val);
    setQueryString({ filter: val });
  };

  const setFilterAircraftExt = (val) => {
    setFilterAircraft(val);
    setQueryString({ aircraft: val });
  };

  const setAirlineCodeExt = (val) => {
    setAirlineCode(val);
    setQueryString({ airline: val });
  };

  useEffect(() => {
    // handle airlineCode change - get data from API

    setLoading(true);
    setInitialFlights([]);
    setFlights([]);
    setAirlineLogo('');
    setFilterAircraft('');
    setFilterText('');

    fetch(`${FLIGHTS_API}${airlineCode}`)
      .then(response => response.json())
      .then((data) => {
        // eslint-disable-next-line no-param-reassign
        data.flights = data.flights.sort((a, b) => {
          // sort by plane and then by reg
          if (a.plane === b.plane) {
            // eslint-disable-next-line no-nested-ternary
            return a.reg > b.reg ? 1 : b.reg > a.reg ? -1 : 0;
          }
          // eslint-disable-next-line no-nested-ternary
          return a.plane > b.plane ? 1 : b.plane > a.plane ? -1 : 0;
        });

        const planes = data.flights.map(fl => fl.plane);

        let aircraftTypesObj = {};
        planes.forEach((plane) => {
          if (plane) {
            if (aircraftTypesObj[plane]) {
              aircraftTypesObj[plane] += 1;
            } else {
              aircraftTypesObj[plane] = 1;
            }
          }
        });

        aircraftTypesObj = { ALL: planes.length, ...aircraftTypesObj };

        setFlights(data.flights);
        setInitialFlights(data.flights);
        setAircraftTypes(aircraftTypesObj);
        setFilterAircraft(initialAircraft ? initialAircraft.toUpperCase() : '');
        if (initialFilterText) {
          setFilterText(initialFilterText);
        }

        setAirlineLogo(AIRLINES.find(airline => airline.code === airlineCode).logo);
        setLastFetch(data.date);
        setLoading(false);
      })
      .catch((err) => {
        // eslint-disable-next-line no-console
        console.log(err);
      });
  }, [airlineCode]);

  useEffect(() => {
    // handle filterText & initialFlights change - filter locally

    let updatedList = initialFlights;

    // filter by filterText (input)
    updatedList = updatedList.filter((item) => {
      const values = Object.values(item);
      let found = false;

      // eslint-disable-next-line no-plusplus
      for (let i = 0; i < values.length; i++) {
        if (
          values[i]
            .toString()
            .toLowerCase()
            .search(filterText.toLowerCase()) !== -1
        ) {
          found = true;
          break;
        }
      }
      return found;
    });

    // filter by aircraft type (radio buttons)
    updatedList = updatedList.filter(item => (
      item.plane
        .toString()
        .toLowerCase()
        .search(filterAircraft.toLowerCase()) !== -1
    ));

    const gfCount = updatedList.filter(flight => flight.details.status.generic.status.color === 'green').length;
    const yfCount = updatedList.filter(flight => flight.details.status.generic.status.color === 'yellow').length;
    const rfCount = updatedList.filter(flight => flight.details.status.generic.status.color === 'red').length;

    setFlights(updatedList);
    setGreenFlightsCount(gfCount);
    setYellowFlightsCount(yfCount);
    setRedFlightsCount(rfCount);
  }, [filterText, filterAircraft, initialFlights]);

  return (
    // eslint-disable-next-line react/jsx-filename-extension
    <div className="container">
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <a className="navbar-brand" href="##"><img src={airlineLogo} alt="" /></a>
        <div className="navbar-nav ml-auto">
          <form>
            <div className="form-row justify-content-end">
              <div className="col-auto">
                <select
                  className="form-control"
                  onChange={e => setAirlineCodeExt(e.target.value)}
                  value={initialAirline}
                  title="Airline"
                  // disabled={(initialAirline) ? 'disabled' : ''}
                >
                  {AIRLINES.map(airline => (
                    <option value={airline.code}>{airline.label}</option>
                  ))}
                </select>
              </div>
              <div className="col-md-3">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Filter"
                  title="Filter text"
                  value={filterText}
                  onChange={e => setFilterTextExt(e.target.value)}
                  // disabled={(initialFilterText) ? 'disabled' : ''}
                />
              </div>
              <div className="col-auto">
                <select
                  className="form-control"
                  title="Aircraft type" 
                  onChange={e => setFilterAircraftExt(e.target.value)}
                  value={filterAircraft}
                  // disabled={(initialAircraft) ? 'disabled' : ''}
                >
                  {Object.keys(aircraftTypes).map((type) => (
                    <option key={type} value={type !== 'ALL' ? type : ''}>
                      {type}
                      &nbsp;
                      ({aircraftTypes[type]})
                    </option>
                  ))}
                </select>
              </div>
            </div>
          </form>
          {/* <form className="form-inline">
            <fieldset className="form-group pr-2">
              <select
                className="form-control"
                onChange={event => setAirlineCode(event.target.value)}
                title="Airline"
              >
                {AIRLINES.map(airline => (
                  <option value={airline.code}>{airline.label}</option>
                ))}
              </select>
            </fieldset>
            <fieldset className="form-group pr-2">
              <input
                type="text"
                className="form-control"
                placeholder="Filter (eg. b788, waw, sp-lwa)"
                onChange={event => setFilterText(event.target.value)}
              />
            </fieldset>
            <fieldset className="form-group pr-2">
              <select className="form-control" onChange={e => setFilterAircraft(e.target.value)}>
                {Object.keys(aircraftTypes).map((type) => (
                  <option value={type !== 'ALL' ? type : ''}>
                    {type}
                    &nbsp;
                    ({aircraftTypes[type]})
                  </option>
                ))}
              </select>
            </fieldset>
          </form> */}
        </div>
        <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            <div className="badge badge-success airline-summary">
              <span>ON TIME</span>
              <br />
              <span className="airline-summary-count">{greenFlightsCount}</span>
            </div>
          </li>
          <li className="nav-item">
            <div className="badge badge-warning airline-summary">
              <span>DELAY</span>
              <br />
              <span className="airline-summary-count">{yellowFlightsCount}</span>
            </div>
          </li>
          <li className="nav-item">
            <div className="badge badge-danger airline-summary">
              <span>DELAY</span>
              <br />
              <span className="airline-summary-count">{redFlightsCount}</span>
            </div>
          </li>
          <li className="nav-item">
            <div className="badge badge-light airline-summary">
              <span>TOTAL</span>
              <br />
              <span className="airline-summary-count">{flights.length}</span>
            </div>
          </li>
        </ul>
      </nav>
      <br />
      {loading ? (
        <Loader type="Grid" color="#28a745" height="100" width="100" />
      ) : (
        <div>
          <table className="table table-hover">
            <thead>
              <tr>
                <th scope="col" title="Callsign">
                  FLIGHT
                </th>
                <th scope="col">AIRCRAFT</th>
                <th scope="col" className="d-none d-lg-block">
                  PHOTO
                </th>
                <th scope="col">FROM</th>
                <th scope="col">TO</th>
                <th scope="col">STATUS</th>
              </tr>
            </thead>
            <tbody>
              {flights.map(flight => (
                <ListItem flight={flight} />
              ))}
            </tbody>
          </table>
          <div>
            <span>
              Last update:
              {' '}
              <b>{moment(lastFetch).format('HH:mm')}</b>
            </span>
          </div>
        </div>
      )}
    </div>
  );
}

export default List;
